# Installs principales

Si faltan los node_modules es normal, porque no estan incluidos con los gitignore, para ello usa los siguientes comandos:

npm install
npm install bootstrap jquery
"node_modules/bootstrap/dist/css/bootstrap.min.css"
ng add @ng-bootstrap/ng-bootstrap

# ANGULAR MATERIALS

ng add @angular/material

# Añadir FIREBASE

npm install firebase @angular/fire --save

const firebaseConfig = {
  apiKey: "AIzaSyC8aZtwfxBnF6w3VqdgmfdZ4KDe4LZ-jYQ",
  authDomain: "pruebas-49aa8.firebaseapp.com",
  projectId: "pruebas-49aa8",
  storageBucket: "pruebas-49aa8.appspot.com",
  messagingSenderId: "233287065176",
  appId: "1:233287065176:web:a3901e01ab18f0445bf42d",
  measurementId: "G-KTWNDJ5GRN"
};
