// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyC8aZtwfxBnF6w3VqdgmfdZ4KDe4LZ-jYQ',
    authDomain: 'pruebas-49aa8.firebaseapp.com',
    projectId: 'pruebas-49aa8',
    storageBucket: 'pruebas-49aa8.appspot.com',
    messagingSenderId: '233287065176',
    appId: '1:233287065176:web:3d75458857aa5cff5bf42d',
    measurementId: 'G-F6M1LSQXDL',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error'; // Included with Angular CLI.
