import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore } from '@angular/fire/compat/firestore';
import { Book } from '../interfaces/book.interface';

@Injectable({
  providedIn: 'root'
})
export class LibrosService {
  books?: Observable<Book[]>;

  constructor(
    private afs: AngularFirestore
  ) {}

   //Crea un nuevo Libro
   public createBook(data:Book) {
    return this.afs.collection('books').add(data);
  }

  public deleteBook(documentId:string) {
    this.afs.collection('books').doc(documentId).delete();
  }

  //Obtiene un Libro
  public getBook(documentId: string) {
    return this.afs.collection('books').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los Libros
  public getBooks() {
    return this.afs.collection('books').snapshotChanges();
  }
  //Actualiza un Libro
  public updateBook(documentId: string, data:Book) {
    return this.afs.collection('books').doc(documentId).set(data);
  }
}
