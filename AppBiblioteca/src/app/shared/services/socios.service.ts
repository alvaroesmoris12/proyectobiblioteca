import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Socio } from '../interfaces/socios.interface';

@Injectable({
  providedIn: 'root'
})
export class SociosService {

  constructor(
    private afs: AngularFirestore
  ) {}

   //Crea un nuevo Socio
  public createSocio(data:Socio) {
    return this.afs.collection('socios').add(data);
  }

  public deleteSocio(documentId:string) {
    this.afs.collection('socios').doc(documentId).delete();
  }

  //Obtiene un Socio
  public getSocio(documentId: string) {
    return this.afs.collection('socios').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los Socio
  public getSocios() {
    return this.afs.collection('socios').snapshotChanges();
  }
  //Actualiza un Socio
  public updateSocio(documentId: string, data:Socio) {
    return this.afs.collection('socios').doc(documentId).set(data);
  }
}
