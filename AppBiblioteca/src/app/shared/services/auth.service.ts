import firebase from 'firebase/compat/app';
import {Injectable, NgZone} from '@angular/core';
import {Router} from "@angular/router";
import {AngularFirestore, AngularFirestoreDocument} from "@angular/fire/compat/firestore";
import {AngularFireAuth} from "@angular/fire/compat/auth";
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userVerify?:any;
  constructor(private afAuth:AngularFireAuth, private router:Router){

  }

  async SignIn(email:string, password:string){
    try {
      return await this.afAuth.signInWithEmailAndPassword(email, password).then(() => {
        this.isLogged().subscribe(result =>{
          this.userVerify = (result !== null && result.emailVerified !== false) ? true:false;
          this.router.navigate(['libros'])
        });
      });
    } catch (err) {
      console.log("Error: " + err);
      return null;
    }
  }

  async SignInWithGoogle(email:string, password:string){
    try {
      return await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(() => {
        this.isLogged().subscribe(result =>{
          this.userVerify = (result !== null && result.emailVerified !== false) ? true:false;
          this.router.navigate(['libros'])
        });
      });
    } catch (err) {
      console.log("Error: " + err);
      return null;
    }
  }

  async SignUp(email:string, password:string){
    try{
      return await this.afAuth.createUserWithEmailAndPassword(email, password).then((result) => {this.SendVerificationMail(); this.LogOut()});
    }catch(err){
      alert("Error: " + err);
      return null;
    }
  }

  isLogged(){
    return this.afAuth.authState;
  }

  get isLoggedIn(){
    try {
      this.isLogged().subscribe(result =>{
        this.userVerify = (result !== null && result.emailVerified !== false) ? true:false;
      });
    } catch (err) {
      console.log("Error: " + err);
      return false;
    }
    return this.userVerify;
  }

  async LogOut(){
    this.afAuth.signOut().then(() => this.router.navigate(['sign-in']));
  }


  SendVerificationMail() {
    console.log(this.afAuth.currentUser);
    return this.afAuth.currentUser
      .then( u => u?.sendEmailVerification())
      .then(() => {
        this.router.navigate(['verify-email-address']);
      })
  }

  ForgotPassword(passwordResetEmail: string) {
    return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
      }).catch((error) => {
        window.alert(error)
      })
  }
}
