import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Prestamo } from '../interfaces/prestamo.interface';

@Injectable({
  providedIn: 'root'
})
export class PrestamosService {

  constructor(
    private afs: AngularFirestore
  ) {}

   //Crea un nuevo Libro
   public createPrestamo(data:Prestamo) {
    return this.afs.collection('prestamos').add(data);
  }

  public deletePrestamo(documentId:string) {
    this.afs.collection('prestamos').doc(documentId).delete();
  }

  //Obtiene un Libro
  public getPrestamo(documentId: string) {
    return this.afs.collection('prestamos').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los Libros
  public getPrestamos() {
    return this.afs.collection('prestamos').snapshotChanges();
  }
  //Actualiza un Libro
  public updatePrestamo(documentId: string, data:Prestamo) {
    return this.afs.collection('prestamos').doc(documentId).set(data);
  }
}
