export interface Socio{
  id: string,
  data:{
    name: string;
    surname: string;
    age: number;
    nick:string;
    password:string;
    img:string;
  }
}
