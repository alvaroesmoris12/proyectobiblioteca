export interface Prestamo{
  id: string,
  data:{
    startDate: Date,
    finishDate: Date,
    idBook:string,
    idSocio:string
  }

}
