export interface Book{
  id: string;
  data:{
    title: string;
    autor: string;
    description: string;
    genre: string;
    cover: string;
    isPrestado:boolean;
  }

}
