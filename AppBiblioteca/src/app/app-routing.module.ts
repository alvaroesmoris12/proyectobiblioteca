import { VerifyEmailComponent } from './views/login/verify-email/verify-email.component';
import { ForgotPasswordComponent } from './views/login/forgot-password/forgot-password.component';
import { HomeComponent } from './views/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { AuthGuard } from './shared/guard/auth.guard';
import { SingInComponent } from './views/login/sing-in/sing-in.component';
import { SingUpComponent } from './views/login/sing-up/sing-up.component';

const routes: Routes = [
  { path: 'sign-in', component: SingInComponent },
  { path: 'register-user', component: SingUpComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent },
  { path: 'home', component:HomeComponent, canActivate:[AuthGuard]},
  { path: 'libros', loadChildren: () => import('./pages/libros/libros.module').then(m => m.LibrosModule), canActivate:[AuthGuard]},
  { path: 'socios', loadChildren: () => import('./pages/socios/socios.module').then(m => m.SociosModule), canActivate:[AuthGuard]},
  { path: 'prestamos', loadChildren: () => import('./pages/prestamos/prestamos.module').then(m => m.PrestamosModule), canActivate:[AuthGuard]},
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  {path:'**', component:PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
