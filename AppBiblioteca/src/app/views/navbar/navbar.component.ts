import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public authService:AuthService, private router:Router) { }

  ngOnInit(): void {
    console.log(this.userLogged);
  }

  userLogged = this.authService.isLogged();

  OnLogOut():void{
    this.authService.LogOut();
    this.router.navigate(['sign-in']);
  }

}
