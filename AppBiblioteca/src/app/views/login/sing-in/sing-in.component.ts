import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-sing-in',
  templateUrl: './sing-in.component.html',
  styleUrls: ['./sing-in.component.css']
})
export class SingInComponent implements OnInit {

  user = {
    email: '',
    password: ''
  }

  constructor(public authService: AuthService, private router:Router) { }

  ngOnInit(): void {}

  OnSignIn():void{
    this.authService.SignIn(this.user.email, this.user.password).then(() => this.router.navigate(['libros']));
  }

  OnSignInWithGoogle():void{
    this.authService.SignInWithGoogle(this.user.email, this.user.password).then(() => this.router.navigate(['libros']));
  }

  OnSignUp(){
    this.router.navigate(['register-user']);
  }
}
