import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Socio } from 'src/app/shared/interfaces/socios.interface';
import { LibrosService } from 'src/app/shared/services/libros.service';
import { PrestamosService } from 'src/app/shared/services/prestamos.service';
import { SociosService } from 'src/app/shared/services/socios.service';

@Component({
  selector: 'app-nuevo-prestamo',
  templateUrl: './nuevo-prestamo.component.html',
  styleUrls: ['./nuevo-prestamo.component.css'],
})
export class NuevoPrestamoComponent implements OnInit {
  pForm: FormGroup;
  books: any[] = [];
  socios: any[] = [];
  bookAux: any = {};
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private bS: LibrosService,
    private pS: PrestamosService,
    private sS: SociosService
  ) {
    this.pForm = this.formBuilder.group({
      startDate: [Date, Validators.required],
      finishDate: [Date, Validators.required],
      idBook: ['', Validators.required],
      idSocio: ['', Validators.required],
    });

    this.sS.getSocios().subscribe((socios) => {
      socios.forEach((socio) => {
        this.socios.push({
          id: socio.payload.doc.id,
          data: socio.payload.doc.data(),
        });
      });
    });

    this.bS.getBooks().subscribe((books) => {
      let auxBooks: any[] = [];
      books.forEach((book) => {
        auxBooks.push({
          id: book.payload.doc.id,
          data: book.payload.doc.data(),
        });
      });

      this.books = [];
      auxBooks.forEach((book) => {
        if (book.data.isPrestado == false) this.books.push(book);
      });

      console.log(this.books);
    });
  }

  ngOnInit(): void {}

  ngOnChanges(): void {}

  onSave(): void {
    // console.log(this.pForm.value);
    let valoresForm = this.pForm.value;
    this.bS.getBook(valoresForm.idBook).subscribe((book) => {
      this.bookAux = {
        id: valoresForm.idBook,
        data: book.payload.data(),
      };

      // console.log(this.bookAux);
      this.bookAux.data.isPrestado = true;

      this.bS.updateBook(this.bookAux.id, this.bookAux.data);
    });
    setTimeout(() => {
      // console.log('Saved: ' + this.pForm.value.toString());
      this.pS.createPrestamo(this.pForm.value);
      this.router.navigate(['/prestamos']);
    }, 500);
  }

  onGoBackToList(): void {
    this.router.navigate(['/prestamos']);
  }
}
