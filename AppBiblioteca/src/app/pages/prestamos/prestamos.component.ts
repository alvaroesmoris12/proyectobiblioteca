import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LibrosService } from 'src/app/shared/services/libros.service';
import { PrestamosService } from 'src/app/shared/services/prestamos.service';

@Component({
  selector: 'app-prestamos',
  templateUrl: './prestamos.component.html',
  styleUrls: ['./prestamos.component.css']
})
export class PrestamosComponent implements OnInit {
  prestamos:any[] = [];
  bookAux:any = {};
  constructor(private pS:PrestamosService, private router:Router, private bS:LibrosService) {}

  ngOnInit(): void {
    this.pS.getPrestamos().subscribe(prestamos =>{
      this.prestamos = [];
      prestamos.forEach(prestamo =>{
        this.prestamos.push({
          id: prestamo.payload.doc.id,
          data: prestamo.payload.doc.data(),
        })

        this.prestamos = this.prestamos.filter(p => p.data.finishDate < Date.now);
      })
    });
  }

  ngOnChanges(){
    this.pS.getPrestamos().subscribe(prestamos =>{
      this.prestamos = [];
      prestamos.forEach(prestamo =>{
        this.prestamos.push({
          id: prestamo.payload.doc.id,
          data: prestamo.payload.doc.data(),
        })

        this.prestamos = this.prestamos.filter(p => p.data.finishDate <= new Date());
      })
    });

    window.location.reload();
  }

  onEditarPrestamo(item: any): void {
    this.router.navigate(['prestamos/editar', item.id]);
  }

  onInfoPrestamo(item: any): void {
    this.router.navigate(['prestamos/info', item.id]);
  }

  onEliminarPrestamo(item: any): void {
    this.bS.getBook(item.data.idBook).subscribe(book => {
      this.bookAux = {
        id: item.data.idBook,
        data: book.payload.data()
      }

      this.bookAux.data.isPrestado = false;
      item.data.finishDate = new Date();
      this.bS.updateBook(this.bookAux.id, this.bookAux.data).then(() => {this.pS.updatePrestamo(item.id, item.data)}).then(() => location.reload());
    });
  }

  onGoToCreate():void{
    this.router.navigate(['prestamos/create']);
  }
}
