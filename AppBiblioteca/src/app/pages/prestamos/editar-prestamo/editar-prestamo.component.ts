import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LibrosService } from 'src/app/shared/services/libros.service';
import { PrestamosService } from 'src/app/shared/services/prestamos.service';
import { SociosService } from 'src/app/shared/services/socios.service';

@Component({
  selector: 'app-editar-prestamo',
  templateUrl: './editar-prestamo.component.html',
  styleUrls: ['./editar-prestamo.component.css']
})
export class EditarPrestamoComponent implements OnInit {

  bookAux:any = {};
  pres?:any;
  pForm:FormGroup;
  books:any[] = [];
  socios:any[] = [];

  constructor(private activatedRoute:ActivatedRoute, private router:Router,
    private formBuilder:FormBuilder, private pS:PrestamosService, private sS:SociosService, private bS:LibrosService) {
      this.pForm = this.formBuilder.group({
        startDate:[Date, Validators.required],
        finishDate:[Date, Validators.required],
        idBook:['', Validators.required],
        idSocio:['', Validators.required],
   });

   this.sS.getSocios().subscribe(socios => {
    socios.forEach(socio => {
      this.socios.push({
        id: socio.payload.doc.id,
        data: socio.payload.doc.data()
      })
    })
  })

  this.bS.getBooks().subscribe(books => {
    books.forEach(book =>{
      this.books.push({
        id: book.payload.doc.id,
        data: book.payload.doc.data()
      })
    })
  })

  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params.id;
      this.pS.getPrestamo(id).subscribe(p => {
        this.pres = {
          id: id,
          data: p.payload.data(),
        }

        this.pForm.patchValue({
          startDate:this.pres.data.startDate,
          finishDate:this.pres.data.finishDate,
          idBook:this.pres.data.idBook,
          idSocio:this.pres.data.idSocio,
        })
      })
    })
  }

  onSave(): void {
    console.log("Saved: " + this.pForm?.value.toString());
    if(this.pForm.valid){
      this.books.forEach(book => book.isPrestado = false);
      this.pForm.value.idBook

      this.bS.getBook(this.pForm.value.idBook).subscribe((book) => {
        this.bookAux = {
          id: this.pForm.value.idBook,
          data: book.payload.data(),
        };

        console.log(this.bookAux);
        //this.bookAux.data.isPrestado = true;

        this.bS.updateBook(this.bookAux.id, this.bookAux.data);
      });
      setTimeout(() =>{
        this.pS.updatePrestamo(this.pres.id, this.pForm.value);
        this.router.navigate(['prestamos/'])
      }, 500);
    }else{
      alert("Todos los campos deben ser rellenados");
    }
  }

  onGoBackToList():void{
    this.router.navigate(['/prestamos']);
  }
}
