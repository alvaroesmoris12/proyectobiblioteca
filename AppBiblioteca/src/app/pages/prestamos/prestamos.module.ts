import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrestamosRoutingModule } from './prestamos-routing.module';
import { PrestamosComponent } from './prestamos.component';
import { NuevoPrestamoComponent } from './nuevo-prestamo/nuevo-prestamo.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InfoPrestamoComponent } from './info-prestamo/info-prestamo.component';
import { EditarPrestamoComponent } from './editar-prestamo/editar-prestamo.component';


@NgModule({
  declarations: [
    PrestamosComponent,
    NuevoPrestamoComponent,
    InfoPrestamoComponent,
    EditarPrestamoComponent
  ],
  imports: [
    CommonModule,
    PrestamosRoutingModule,
    ReactiveFormsModule
  ]
})
export class PrestamosModule { }
