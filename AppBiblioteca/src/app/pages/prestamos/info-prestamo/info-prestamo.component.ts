import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LibrosService } from 'src/app/shared/services/libros.service';
import { PrestamosService } from 'src/app/shared/services/prestamos.service';
import { SociosService } from 'src/app/shared/services/socios.service';

@Component({
  selector: 'app-info-prestamo',
  templateUrl: './info-prestamo.component.html',
  styleUrls: ['./info-prestamo.component.css'],
})
export class InfoPrestamoComponent implements OnInit {
  prestamo: any = {};
  book: any = {};
  socio: any = {};
  bookAux: any = {};
  constructor(
    private router: Router,
    private pS: PrestamosService,
    private sS: SociosService,
    private bS: LibrosService,
    private aR: ActivatedRoute
  ) {};

  ngOnInit(): void {
    this.aR.params.subscribe((params) => {
      let id = params.id;
      this.pS.getPrestamo(id).subscribe((p) => {
        this.prestamo = {
          id: id,
          data: p.payload.data(),
        };

        let idSocio = this.prestamo.data.idSocio;
        let idBook = this.prestamo.data.idBook;
        this.bS.getBook(idBook).subscribe((b) => {
          this.book = {
            id: idBook,
            data: b.payload.data(),
          };
        });

        this.sS.getSocio(idSocio).subscribe((s) => {
          this.socio = {
            id: idSocio,
            data: s.payload.data(),
          };
        }); // * Tengo que meter los observables unos dentro de otros para utilizar sus datos, si no seran no asignados
      });

      // console.log(this.book);
      // console.log(this.socio);
    });

    // if (this.prestamo != null && this.prestamo != undefined) {
    //   let idSocio = this.prestamo.data.idSocio;
    //   let idBook = this.prestamo.data.idBook;

    //   console.log(idSocio);
    //   console.log(idBook);

    //   this.bS.getBook(idBook).subscribe((b) => {
    //     this.book = {
    //       id: idBook,
    //       data: b.payload.data(),
    //     };
    //   });

    //   this.sS.getSocio(idSocio).subscribe((s) => {
    //     this.socio = {
    //       id: idSocio,
    //       data: s.payload.data(),
    //     };
    //   });
    // } // * Para que salgan los socios y libros aun no se ha hayado respuesta
  };

  onGoToSocio(): void {
    this.router.navigate(['/socios/info', this.prestamo.data.idSocio]);
  };

  onGoToBook(): void {
    this.router.navigate(['/libros/info', this.prestamo.data.idBook]);
  };

  onEditarPrestamo(item: any): void {
    this.router.navigate(['/prestamos/editar', item.id]);
  };

  onEliminarPrestamo(item: any): void {
    this.bS.getBook(item.data.idBook).subscribe(book => {
      this.bookAux = {
        id: item.data.idBook,
        data: book.payload.data()
      }

      console.log(this.bookAux);
      this.bookAux.data.isPrestado = false;
      item.data.finishDate = Date.now;
      this.bS.updateBook(this.bookAux.id, this.bookAux.data).then(() => {this.pS.updatePrestamo(item.id, item.data)});
    });
  };

  onGoBackToList(): void {
    this.router.navigate(['/prestamos']);
  };
}
