import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarPrestamoComponent } from './editar-prestamo/editar-prestamo.component';
import { InfoPrestamoComponent } from './info-prestamo/info-prestamo.component';
import { NuevoPrestamoComponent } from './nuevo-prestamo/nuevo-prestamo.component';
import { PrestamosComponent } from './prestamos.component';

const routes: Routes = [
  {path: 'create', component:NuevoPrestamoComponent},
  {path: 'info/:id', component:InfoPrestamoComponent},
  {path: 'editar/:id', component:EditarPrestamoComponent},
  { path: '', component: PrestamosComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrestamosRoutingModule { }
