import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PrestamosService } from 'src/app/shared/services/prestamos.service';
import { SociosService } from 'src/app/shared/services/socios.service';
import { DeleteDialogSocioComponent } from '../delete-dialog-socio/delete-dialog-socio.component';

@Component({
  selector: 'app-lista-socio',
  templateUrl: './lista-socio.component.html',
  styleUrls: ['./lista-socio.component.css']
})
export class ListaSocioComponent implements OnInit {

  socios: any[] = [];
  prestamos: any[] = [];
  idSocios: string[] = [];
  // filterpost = '';
  constructor(
    private router: Router,
    private sociosService: SociosService,
    private pS: PrestamosService, private dialog:MatDialog
  ) {
    this.sociosService.getSocios().subscribe((socios) => {
      console.log(socios);
      this.socios = [];
      socios.forEach((socio) =>
        this.socios.push({
          id: socio.payload.doc.id,
          data: socio.payload.doc.data(),
        })
      );
    });
  }

  ngOnInit(): void {
    this.pS.getPrestamos().subscribe((prestamos) => {
      prestamos.forEach((prestamo) =>
        this.prestamos.push({
          id: prestamo.payload.doc.id,
          data: prestamo.payload.doc.data(),
        })
      );

      this.prestamos.forEach((prestamos) => {
        if(!this.prestamos.includes(prestamos.data.idSocio))
          this.idSocios.push(prestamos.data.idSocio)
      }
      );

      console.log(this.idSocios);
    });
  }

  ngOnChanges() {
    this.socios = [];
    this.sociosService.getSocios().subscribe(socios => {
      socios.forEach(soc => this.socios.push({
        id: soc.payload.doc.id,
        data: soc.payload.doc.data()
      }));
    })

    window.location.reload();
  }

  onEditarSocio(item: any): void {
    this.router.navigate(['socios/editar', item.id]);
  }

  onInfoSocio(item: any): void {
    this.router.navigate(['socios/info', item.id]);
  }

  onGoToCreate(): void {
    this.router.navigate(['socios/create']);
  }

  openDialog(item:any) {
    const dialogRef = this.dialog.open(DeleteDialogSocioComponent, {data: {...item}});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
  });}
}
