import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SociosService } from 'src/app/shared/services/socios.service';

@Component({
  selector: 'app-nuevo-socio',
  templateUrl: './nuevo-socio.component.html',
  styleUrls: ['./nuevo-socio.component.css']
})
export class NuevoSocioComponent implements OnInit {

  sociosForm:FormGroup;
  constructor(private router:Router, private formBuilder:FormBuilder, private sociosService:SociosService) {
    this.sociosForm = this.formBuilder.group({
      name:['', Validators.required],
      surname:['', Validators.required],
      age:[0, Validators.required],
      nick:['', Validators.required],
      password:['', Validators.required],
      img:['', Validators.required]
    })}

  ngOnInit(): void {
  }

  onSave(): void {
    console.log("Saved: " + this.sociosForm.value.toString());
    if(this.sociosForm.valid){
      this.sociosService.createSocio(this.sociosForm.value);
      this.router.navigate(['/socios'])
    } else{
      alert("Hacen falta todos los campos")
    }

  }

  onGoBackToList():void{
    this.router.navigate(['/socios']);
  }

}
