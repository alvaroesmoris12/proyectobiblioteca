import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsSocioComponent } from './cards-socio.component';

describe('CardsSocioComponent', () => {
  let component: CardsSocioComponent;
  let fixture: ComponentFixture<CardsSocioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardsSocioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsSocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
