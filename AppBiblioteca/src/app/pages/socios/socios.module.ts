import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SociosRoutingModule } from './socios-routing.module';
import { SociosComponent } from './socios.component';
import { NuevoSocioComponent } from './nuevo-socio/nuevo-socio.component';
import { InfoSocioComponent } from './info-socio/info-socio.component';
import { EditarSocioComponent } from './editar-socio/editar-socio.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeleteDialogSocioComponent } from './delete-dialog-socio/delete-dialog-socio.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ListaSocioComponent } from './lista-socio/lista-socio.component';
import { CardsSocioComponent } from './cards-socio/cards-socio.component';


@NgModule({
  declarations: [
    SociosComponent,
    NuevoSocioComponent,
    InfoSocioComponent,
    EditarSocioComponent,
    DeleteDialogSocioComponent,
    ListaSocioComponent,
    CardsSocioComponent
  ],
  imports: [
    CommonModule,
    SociosRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule
  ]
})
export class SociosModule { }
