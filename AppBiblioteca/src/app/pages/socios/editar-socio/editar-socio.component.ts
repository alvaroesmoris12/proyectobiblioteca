import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SociosService } from 'src/app/shared/services/socios.service';
import { ThisReceiver } from '@angular/compiler';

@Component({
  selector: 'app-editar-socio',
  templateUrl: './editar-socio.component.html',
  styleUrls: ['./editar-socio.component.css'],
})
export class EditarSocioComponent implements OnInit {
  socio?: any;
  sociosForm: FormGroup;
  private isEmail = /\S*@\S*\.\S/; /* Para emails */
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private sociosService: SociosService
  ) {
    this.sociosForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      age: [0, Validators.required],
      nick: ['', Validators.required],
      password: ['', Validators.required],
      img: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      let socioId = params.id;
      console.log(socioId);
      this.sociosService.getSocio(socioId).subscribe((socio) => {
        console.log(socio);
        this.socio = {
          id: socioId,
          data: socio.payload.data(),
        };
        console.log(this.socio);
        this.sociosForm.patchValue({
          name: this.socio.data.name,
          surname: this.socio.data.surname,
          age: this.socio.data.age,
          inscriptionDate: this.socio.data.inscriptionDate,
          nick: this.socio.data.nick,
          password: this.socio.data.password,
          img: this.socio.data.img,
        });
      });
    });
    // this.initForm();
  }

  onSave(): void {
    console.log('Saved: ' + this.sociosForm?.value.toString());
    if (this.sociosForm.valid) {
      this.sociosService.updateSocio(this.socio.id, this.sociosForm.value);
      this.router.navigate(['socios/']);
    } else {
      alert('Todos los campos deben ser rellenados');
    }
  }

  onGoBackToList(): void {
    this.router.navigate(['/socios']);
  }
}
