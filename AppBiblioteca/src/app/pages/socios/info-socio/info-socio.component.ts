import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SociosService } from 'src/app/shared/services/socios.service';
import { PrestamosService } from 'src/app/shared/services/prestamos.service';
import { DeleteDialogSocioComponent } from '../delete-dialog-socio/delete-dialog-socio.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-info-socio',
  templateUrl: './info-socio.component.html',
  styleUrls: ['./info-socio.component.css']
})
export class InfoSocioComponent implements OnInit {
  socio?:any;
  prestamos: any[] = [];
  idSocios: string[] = [];
  constructor(private activatedRoute:ActivatedRoute, private router:Router,
    private sociosService:SociosService, private pS:PrestamosService, private dialog:MatDialog) {

   }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      let socioId = params.id;
      console.log(socioId);
      this.sociosService.getSocio(socioId).subscribe(socio => {
        console.log(socio);
        this.socio = {
          id:socioId,
          data: socio.payload.data()
        }
        console.log(this.socio);
      });
    });


    this.pS.getPrestamos().subscribe((prestamos) => {
      prestamos.forEach((prestamo) =>
        this.prestamos.push({
          id: prestamo.payload.doc.id,
          data: prestamo.payload.doc.data(),
        })
      );

      this.prestamos.forEach((prestamos) => {
        if(!this.prestamos.includes(prestamos.data.idSocio))
          this.idSocios.push(prestamos.data.idSocio)
      }
      );

      console.log(this.idSocios);
    });
  }

  onEditarSocio(item: any): void {
    this.router.navigate(['/socios/editar', item.id]);
  }

  onGoBackToList():void{
    this.router.navigate(['/socios']);
  }

  openDialog(item:any) {
    const dialogRef = this.dialog.open(DeleteDialogSocioComponent, {data: {...item}});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
  });}

}
