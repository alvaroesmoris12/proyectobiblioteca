import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsSocioComponent } from './cards-socio/cards-socio.component';
import { EditarSocioComponent } from './editar-socio/editar-socio.component';
import { InfoSocioComponent } from './info-socio/info-socio.component';
import { ListaSocioComponent } from './lista-socio/lista-socio.component';
import { NuevoSocioComponent } from './nuevo-socio/nuevo-socio.component';
import { SociosComponent } from './socios.component';

const routes: Routes = [
  {path: 'info/:id', component:InfoSocioComponent},
  {path: 'editar/:id', component:EditarSocioComponent},
  {path:'create', component:NuevoSocioComponent},
  { path: '', component: SociosComponent, children: [
    {path:'list', component:ListaSocioComponent},
    {path:'cards', component:CardsSocioComponent},
    {path:'', component:ListaSocioComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SociosRoutingModule { }
