import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDialogSocioComponent } from './delete-dialog-socio.component';

describe('DeleteDialogSocioComponent', () => {
  let component: DeleteDialogSocioComponent;
  let fixture: ComponentFixture<DeleteDialogSocioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteDialogSocioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDialogSocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
