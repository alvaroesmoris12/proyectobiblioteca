import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PrestamosService } from 'src/app/shared/services/prestamos.service';
import { SociosService } from 'src/app/shared/services/socios.service';

@Component({
  selector: 'app-delete-dialog-socio',
  templateUrl: './delete-dialog-socio.component.html',
  styleUrls: ['./delete-dialog-socio.component.css']
})
export class DeleteDialogSocioComponent implements OnInit {

  prestamos: any[] = [];
  idSocios: string[] = [];
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private sociosService: SociosService, private pS: PrestamosService) { }

  ngOnInit(): void {
    this.pS.getPrestamos().subscribe((prestamos) => {
      prestamos.forEach((prestamo) =>
        this.prestamos.push({
          id: prestamo.payload.doc.id,
          data: prestamo.payload.doc.data(),
        })
      );

      this.prestamos.forEach((prestamos) => {
        if(!this.prestamos.includes(prestamos.data.idSocio))
          this.idSocios.push(prestamos.data.idSocio)
      }
      );

      console.log(this.idSocios);
    });
  }

  onEliminarSocio(item: any): void {
    if(this.idSocios.includes(item.id)){
      alert("Sorry you can't delete this member he/she is in a borrow");
    } else
      this.sociosService.deleteSocio(item.id);
  }
}
