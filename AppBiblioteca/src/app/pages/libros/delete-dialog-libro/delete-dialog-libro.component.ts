import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LibrosService } from 'src/app/shared/services/libros.service';
import { ListaLibroComponent } from '../lista-libro/lista-libro.component';

@Component({
  selector: 'app-delete-dialog-libro',
  templateUrl: './delete-dialog-libro.component.html',
  styleUrls: ['./delete-dialog-libro.component.css']
})
export class DeleteDialogLibroComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private librosService:LibrosService) { }

  ngOnInit(): void {}

  onEliminarLibro(item: any): void {
    if(item.data.isPrestado == true){
      alert("You can't delete this book it is borrowed");
    } else
      this.librosService.deleteBook(item.id);
  };
}
