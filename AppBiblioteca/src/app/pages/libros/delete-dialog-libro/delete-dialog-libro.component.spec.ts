import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDialogLibroComponent } from './delete-dialog-libro.component';

describe('DeleteDialogLibroComponent', () => {
  let component: DeleteDialogLibroComponent;
  let fixture: ComponentFixture<DeleteDialogLibroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteDialogLibroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDialogLibroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
