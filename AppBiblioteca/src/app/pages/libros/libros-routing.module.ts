import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsLibroComponent } from './cards-libro/cards-libro.component';
import { EditarLibroComponent } from './editar-libro/editar-libro.component';
import { InfoLibroComponent } from './info-libro/info-libro.component';
import { LibrosComponent } from './libros.component';
import { ListaLibroComponent } from './lista-libro/lista-libro.component';
import { NuevoLibroComponent } from './nuevo-libro/nuevo-libro.component';

const routes: Routes = [
  {path: 'info/:id', component:InfoLibroComponent},
  {path: 'editar/:id', component:EditarLibroComponent},
  {path:'create', component:NuevoLibroComponent},
  {path: '', component: LibrosComponent, children: [
    {path: 'list', component:ListaLibroComponent},
    {path: 'cards', component:CardsLibroComponent},
    {path: '', component:ListaLibroComponent},
  ]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibrosRoutingModule { }
