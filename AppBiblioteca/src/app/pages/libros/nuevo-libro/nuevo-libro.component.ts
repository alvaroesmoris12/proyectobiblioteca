import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LibrosService } from 'src/app/shared/services/libros.service';

@Component({
  selector: 'app-nuevo-libro',
  templateUrl: './nuevo-libro.component.html',
  styleUrls: ['./nuevo-libro.component.css']
})
export class NuevoLibroComponent implements OnInit {

  bookForm:FormGroup;
  constructor(private router:Router, private formBuilder:FormBuilder, private bookservice:LibrosService) {
    this.bookForm = this.formBuilder.group({
      title:['', Validators.required],
      autor:['', Validators.required],
      genre:['', Validators.required],
      description:['', Validators.required],
      cover:['', Validators.required],
      isPrestado:false,
    })}

  ngOnInit(): void {
  }

  onSave(): void {
    console.log("Saved: " + this.bookForm.value.toString());
    this.bookservice.createBook(this.bookForm.value);
    this.router.navigate(['/libros'])
  }

  onGoBackToList():void{
    this.router.navigate(['/libros']);
  }
}
