
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibrosRoutingModule } from './libros-routing.module';
import { LibrosComponent } from './libros.component';
import { InfoLibroComponent } from './info-libro/info-libro.component';
import { EditarLibroComponent } from './editar-libro/editar-libro.component';
import { NuevoLibroComponent } from './nuevo-libro/nuevo-libro.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ListaLibroComponent } from './lista-libro/lista-libro.component';
import { CardsLibroComponent } from './cards-libro/cards-libro.component';

/* Angular Materials */
import {MatDialogModule} from '@angular/material/dialog';
import { DeleteDialogLibroComponent } from './delete-dialog-libro/delete-dialog-libro.component';

@NgModule({
  declarations: [
    LibrosComponent,
    InfoLibroComponent,
    EditarLibroComponent,
    NuevoLibroComponent,
    ListaLibroComponent,
    CardsLibroComponent,
    DeleteDialogLibroComponent
  ],
  imports: [
    CommonModule,
    LibrosRoutingModule,
    ReactiveFormsModule,
    MatDialogModule
  ],
  providers: []
})
export class LibrosModule { }
