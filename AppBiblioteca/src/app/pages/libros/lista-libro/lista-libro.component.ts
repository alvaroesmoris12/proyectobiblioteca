import { DeleteDialogLibroComponent } from './../delete-dialog-libro/delete-dialog-libro.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { LibrosService } from 'src/app/shared/services/libros.service';

@Component({
  selector: 'app-lista-libro',
  templateUrl: './lista-libro.component.html',
  styleUrls: ['./lista-libro.component.css']
})
export class ListaLibroComponent implements OnInit {

  books:any[] = [];
  constructor(private router: Router, private librosService: LibrosService, public dialog:MatDialog) {
    this.librosService.getBooks().subscribe(books => {
      console.log(books)
      this.books = [];
      books.forEach(book => this.books.push({
        id: book.payload.doc.id,
        data: book.payload.doc.data()
      }));
    });
  }

  ngOnInit(): void {

  }

  ngOnChanges(){
    this.books = [];
    this.librosService.getBooks().subscribe(books => {
      books.forEach(book => this.books.push({
        id: book.payload.doc.id,
        data: book.payload.doc.data()
      }));
    });

    window.location.reload();
  }

  onEditarLibro(item: any): void {
    if(item.data.isPrestado == true){
      alert("You can't Edit this book it is borrowed");
    } else
      this.router.navigate(['libros/editar', item.id]);
  }

  onInfoLibro(item: any): void {
    this.router.navigate(['libros/info', item.id]);
  }

  // onEliminarLibro(item: any): void {
  //   if(item.data.isPrestado == true){
  //     alert("You can't delete this book it is borrowed");
  //   } else
  //     this.librosService.deleteBook(item.id);
  // }; Ahora esta en dialog

  onGoToCreate():void{
    this.router.navigate(['libros/create']);
  };

  openDialog(item:any) {
    const dialogRef = this.dialog.open(DeleteDialogLibroComponent, {data: {...item}});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
  });}
}
