import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LibrosService } from 'src/app/shared/services/libros.service';

@Component({
  selector: 'app-editar-libro',
  templateUrl: './editar-libro.component.html',
  styleUrls: ['./editar-libro.component.css']
})
export class EditarLibroComponent implements OnInit {
  book?:any;
  bookForm:FormGroup;
  private isEmail = /\S*@\S*\.\S/; /* Para emails */
  constructor(private activatedRoute:ActivatedRoute, private router:Router,
    private formBuilder:FormBuilder, private bookService:LibrosService) {
      this.bookForm = this.formBuilder.group({
        title:['', Validators.required],
        autor:['', Validators.required],
        genre:['', Validators.required],
        description:['', Validators.required],
        cover:['', Validators.required]
      })
   }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      let bookId = params.id;
      console.log(bookId);
      this.bookService.getBook(bookId).subscribe(book => {
        console.log(book);
        this.book = {
          id:bookId,
          data: book.payload.data()
        }
        console.log(this.book);
        this.bookForm.patchValue({
          title:this.book.data.title,
          autor:this.book.data.autor,
          genre:this.book.data.genre,
          description:this.book.data.description,
          cover:this.book.data.cover
        });
      });
    })
    // this.initForm();

    if (typeof this.book === undefined){
      this.router.navigate(['libros/create'])
    } else{
      this.bookForm?.patchValue(this.book);
      this.bookForm.setValue({
        title:this.book.data.title,
        autor:this.book.data.autor,
        genre:this.book.data.genre,
        description:this.book.data.description,
        cover:this.book.data.cover
      });
    }
  }

  onSave(): void {
    console.log("Saved: " + this.bookForm?.value.toString());
    if(this.bookForm.valid){
      this.bookService.updateBook(this.book.id, this.bookForm.value);
      this.router.navigate(['libros/'])
    }else{
      alert("Todos los campos deben ser rellenados");
    }
  }

  onGoBackToList():void{
    this.router.navigate(['/libros']);
  }

  // private initForm(): void {
  //   this.bookForm = this.formBuilder.group({
  //     title:['', Validators.required],
  //     autor:['', Validators.required],
  //     editionDate:['', Validators.required, Validators.pattern("dd/mm/yyyy")],
  //     description:['', Validators.required],
  //     cover:['', Validators.required]
  //   })
  // }
}
