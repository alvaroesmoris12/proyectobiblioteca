import { Book } from './../../shared/interfaces/book.interface';
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { EJEMPLOS } from 'src/assets/mock';
import { LibrosService } from 'src/app/shared/services/libros.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css'],
})
export class LibrosComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {

  }

  onGoToCreate():void{
    this.router.navigate(['libros/create']);
  };
}
