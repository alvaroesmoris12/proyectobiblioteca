import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { LibrosService } from 'src/app/shared/services/libros.service';
import { DeleteDialogLibroComponent } from '../delete-dialog-libro/delete-dialog-libro.component';

@Component({
  selector: 'app-info-libro',
  templateUrl: './info-libro.component.html',
  styleUrls: ['./info-libro.component.css']
})
export class InfoLibroComponent implements OnInit {
  book?:any;
  constructor(private activatedRoute:ActivatedRoute, private router:Router,
    private bookService:LibrosService, private dialog:MatDialog) {

   }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      let bookId = params.id;
      console.log(bookId);
      this.bookService.getBook(bookId).subscribe(book => {
        console.log(book);
        this.book = {
          id:bookId,
          data: book.payload.data()
        }
        console.log(this.book);
      });
    })
  }

  onEditarLibro(item: any): void {
    if(item.data.isPrestado == true){
      alert("You can't Edit this book it is borrowed");
    } else
      this.router.navigate(['libros/editar', item.id]);
  }

  onGoBackToList():void{
    this.router.navigate(['/libros']);
  }

  openDialog(item:any) {
    const dialogRef = this.dialog.open(DeleteDialogLibroComponent, {data: {...item}});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
  });}

}
